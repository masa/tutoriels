# Tutoriels

Tutoriels du consortium Masa

- Mapping 3M ([pdf](https://gitlab.huma-num.fr/masa/tutoriels/-/raw/main/Tutoriel_MASA_Mapping_3M.pdf))
- Mapping Ontop ([pdf](https://gitlab.huma-num.fr/masa/tutoriels/-/raw/main/Tutoriel_MASA_Mapping_Ontop.pdf))
- [Vocabulaire GeoNames](https://gitlab.huma-num.fr/masa/tutoriels/-/raw/main/Tuto_geonames.html) ([pdf](https://gitlab.huma-num.fr/masa/tutoriels/-/raw/main/Tutoriel_MASA_Vocabulaire_GeoNames.pdf))
- [Vocabulaire Getty AAT](https://gitlab.huma-num.fr/masa/tutoriels/-/raw/main/AAT.html) ([pdf](https://gitlab.huma-num.fr/masa/tutoriels/-/raw/main/Tutoriel_MASA_Vocabulaire_Getty_AAT.pdf))
- [Vocabulaire Pactols](https://gitlab.huma-num.fr/masa/tutoriels/-/raw/main/tuto_pactols.html) ([pdf](https://gitlab.huma-num.fr/masa/tutoriels/-/raw/main/Tutoriel_MASA_Vocabulaire_PACTOLS.pdf))
- [Vocabulaire VIAF](https://gitlab.huma-num.fr/masa/tutoriels/-/raw/main/tuto_VIAF.html) ([pdf](https://gitlab.huma-num.fr/masa/tutoriels/-/raw/main/Tutoriel_MASA_Vocabulaire_VIAF.pdf))

[https://masa.gitpages.huma-num.fr/tutoriels/](https://masa.gitpages.huma-num.fr/tutoriels/)